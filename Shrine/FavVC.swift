//
//  FavVC.swift
//  Shrine
//
//  Created by Ozaki Kester on 2019-03-24.
//  Copyright © 2019 Google. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents

class FavVC: UICollectionViewController {
    //var shouldDisplayLogin = true
    var appBarViewController = MDCAppBarViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.tintColor = .black
        self.view.backgroundColor = .white
        
        self.title = "Избранное"
        
        self.collectionView?.backgroundColor = .white
        
        // AppBar Init
        self.addChildViewController(self.appBarViewController)
        self.view.addSubview(self.appBarViewController.view)
        self.appBarViewController.didMove(toParentViewController: self)
        
        // Set the tracking scroll view.
        self.appBarViewController.headerView.trackingScrollView = self.collectionView
        
        // Setup Navigation Items
        let menuItemImage = UIImage(named: "MenuItem")
        let templatedMenuItemImage = menuItemImage?.withRenderingMode(.alwaysTemplate)
        let menuItem = UIBarButtonItem(image: templatedMenuItemImage,
                                       style: .plain,
                                       target: self,
                                       action:  #selector(menuItemTapped(sender:)))
        self.navigationItem.leftBarButtonItem = menuItem
        
        let tuneItemImage = UIImage(named: "TuneItem")
        let templatedTuneItemImage = tuneItemImage?.withRenderingMode(.alwaysTemplate)
        let tuneItem = UIBarButtonItem(image: templatedTuneItemImage,
                                       style: .plain,
                                       target: nil,
                                       action: nil)
        self.navigationItem.rightBarButtonItems = [ tuneItem]
        
        // TODO: Theme our interface with our colors
        
        // TODO: Theme our interface with our typography
        
        // TODO: Set layout to our custom layout
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (self.collectionViewLayout is UICollectionViewFlowLayout) {
            let flowLayout = self.collectionViewLayout as! UICollectionViewFlowLayout
            let HORIZONTAL_SPACING: CGFloat = 8.0
            let itemDimension: CGFloat = (self.view.frame.size.width - 3.0 * HORIZONTAL_SPACING)
            let itemSize = CGSize(width: itemDimension, height: itemDimension * 0.5)
            flowLayout.itemSize = itemSize
        }
        
        //    if (self.shouldDisplayLogin) {
         //     let thirdViewController = ThirdViewController(nibName: nil, bundle: nil)
         //    self.present(thirdViewController, animated: false, completion: nil)
        //      self.shouldDisplayLogin = false
        //    }
    }
    
    //MARK - Methods
      @objc func menuItemTapped(sender: Any) {
        let ThirdViewController = storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as! ThirdViewController
        present(ThirdViewController, animated: true, completion: nil)
      }
    
    //MARK - UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        let count = Catalog.count
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: "ProductCell",
                                                            for: indexPath) as! ProductCell
        let product = Catalog.productAtIndex(index: indexPath.row)
        cell.imageView.image = UIImage(named: product.imageName)
        cell.nameLabel.text = product.productName
        cell.priceLabel.text = product.price
        return cell
    }
    
}

//MARK: - UIScrollViewDelegate

// The following four methods must be forwarded to the tracking scroll view in order to implement
// the Flexible Header's behavior.

extension FavVC {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView == self.appBarViewController.headerView.trackingScrollView) {
            self.appBarViewController.headerView.trackingScrollDidScroll()
        }
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView == self.appBarViewController.headerView.trackingScrollView) {
            self.appBarViewController.headerView.trackingScrollDidEndDecelerating()
        }
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView,
                                           willDecelerate decelerate: Bool) {
        let headerView = self.appBarViewController.headerView
        if (scrollView == headerView.trackingScrollView) {
            headerView.trackingScrollDidEndDraggingWillDecelerate(decelerate)
        }
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                            withVelocity velocity: CGPoint,
                                            targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let headerView = self.appBarViewController.headerView
        if (scrollView == headerView.trackingScrollView) {
            headerView.trackingScrollWillEndDragging(withVelocity: velocity,
                                                     targetContentOffset: targetContentOffset)
        }
    }
    
}
