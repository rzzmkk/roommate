//
//  LoginVC.swift
//  Shrine
//
//  Created by Ozaki Kester on 2019-03-24.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func loginTapped(_ sender: Any) {
        let HomeViewController = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        present(HomeViewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
